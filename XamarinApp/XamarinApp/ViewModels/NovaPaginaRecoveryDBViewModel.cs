﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using XamarinApp.Models;
using XamarinApp.Util;

namespace XamarinApp.ViewModels
{
    public class NovaPaginaRecoveryDBViewModel : BaseViewModel
    {
        private List<User> usuarios;
        public List<User> Usuarios { get { return usuarios; } set { SetProperty(ref usuarios, value); } }

        protected override async void CurrentPageOnAppearing(object sender, EventArgs eventArgs)
        {
            CarregarUserDB();
        }

        public async void CarregarUserDB()
        {
            var bd = Contexto.GetInstance().dataBase;

            var allUsers = bd.Table<User>();
            Usuarios = new List<User>();
            Usuarios.AddRange(allUsers);
        }
    }
}
