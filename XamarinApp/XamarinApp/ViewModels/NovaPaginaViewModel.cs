﻿using Refit;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Xamarin.Forms;
using XamarinApp.Models;
using XamarinApp.Services;
using XamarinApp.Util;

namespace XamarinApp.ViewModels
{
    public class NovaPaginaViewModel : BaseViewModel
    {
        public NovaPaginaViewModel()
        {
            ItemSelecionadoCommand = new Command(SelecionarItem);
        }

        private User _itemSelecionado;
        public User ItemSelecionado
        {
            get { return _itemSelecionado; }
            set { SetProperty<User>(ref _itemSelecionado, value); }
        }

        private ObservableCollection<User> usuarios;
        public ObservableCollection<User> Usuarios { get { return usuarios; } set { SetProperty(ref usuarios, value); } }

        protected override async void CurrentPageOnAppearing(object sender, EventArgs eventArgs)
        {
            CarregarUser();
            ToastService.ShortAlert("Voltei...");
        }

        protected override async void CurrentPageOnDisappearing(object sender, EventArgs eventArgs)
        {
            ToastService.ShortAlert("Já Fui...");
        }

        public async void CarregarUser()
        {
            var url = "https://jsonplaceholder.typicode.com";
            var retorno = await RestService.For<IRestAPI>(url).SelecionarTodos();
            MessageService.ShowAsync("Carregou o total de: " + retorno.Count + " itens");
            Usuarios = new ObservableCollection<User>();
            foreach (var user in retorno)
                Usuarios.Add(user);
        }

        public void SelecionarItem()
        {
            //MessageService.ShowAsync("Selecionei o item:" + ItemSelecionado.title);
            var bd = Contexto.GetInstance().dataBase;
            bd.Insert(ItemSelecionado);

            var novo = bd.Table<User>().Where(p => p.id == ItemSelecionado.id).FirstOrDefault();
            MessageService.ShowAsync("Novo registro incluído no banco:" + novo.title);
        }
    }
}
