﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace XamarinApp.ViewModels
{
    public class MainPageViewModel : BaseViewModel
    {
        public MainPageViewModel()
        {
            MensagemCommand = new Command(MostrarMensagem);
            LimparCommand = new Command(Limpar);
            NovaPaginaCommand = new Command(NovaPagina);
            ListarUsersCommand = new Command(MostrarUsersDB);
        }
        private string _mensagem;
        public string Mensagem
        {
            get { return _mensagem; }
            set { SetProperty(ref _mensagem, value); }
        }

        public ICommand MensagemCommand { get; set; }

        public ICommand LimparCommand { get; set; }

        public ICommand NovaPaginaCommand { get; set; }

        public object MessageToast { get; private set; }

        public ICommand ListarUsersCommand { get; set; }


        public async void MostrarMensagem()
        {
            Mensagem = "Mensagem exibida com sucesso";
            ToastService.ShortAlert("OK! Tudo certinho");
        }

        public async void Limpar()
        {
            Mensagem = "Clique no botão para ver a mensagem";
            ToastService.LongAlert("Teste botão limpar!!!!");
        }

        private void NovaPagina()
        {
            NavigationService.NavigateToAsync<NovaPaginaViewModel>("OK. Tudo certinho..", false);
        }

        private void MostrarUsersDB()
        {
            NavigationService.NavigateToAsync<NovaPaginaRecoveryDBViewModel>();
        }
    }
}
