﻿using Refit;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using XamarinApp.Models;

namespace XamarinApp.Services
{
    public interface IRestAPI
    {
        [Get("/todos")]
        Task<ICollection<User>> SelecionarTodos();

        [Post("/todos/{id}")]
        Task<ICollection<User>> SelecionarUserPorID([Body]int id);

    }
}
