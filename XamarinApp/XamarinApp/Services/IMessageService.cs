﻿using System.Threading.Tasks;

namespace XamarinApp.Services
{
    public interface IMessageService
    {
        Task ShowAsync(string message);
        Task<bool> ShowAlertConfirmationAsync(string message);
    }
}
