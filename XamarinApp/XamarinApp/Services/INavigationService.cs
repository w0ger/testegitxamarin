﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using XamarinApp.ViewModels;

namespace XamarinApp.Services
{
    public interface INavigationService
    {
        Task PopAsync();
        Task NavigateToAsync<TViewModel>() where TViewModel : BaseViewModel;
        Task NavigateToAsync<TViewModel>(object parameters, bool modal = false) where TViewModel : BaseViewModel;        
    }
}
