﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XamarinApp.Models
{
    public class User
    {
        public int userId { get; set; }
        public int id { get; set; }
        public string title { get; set; }
        public bool completed { get; set; }

        public DateTime dateCreated { get; set; }
    }
}
