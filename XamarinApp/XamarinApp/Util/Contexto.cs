﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using XamarinApp.Models;
using XamarinApp.Services;

namespace XamarinApp.Util
{
    public class Contexto
    {
        private static Contexto instance;
        internal SQLiteConnection dataBase;

        private Contexto()
        {
            dataBase = DependencyService.Get<ISQLite>().GetConnection();

            dataBase.CreateTable<User>();
        }

        public static Contexto GetInstance()
        {
            return instance ?? (instance = new Contexto());
        }
    }
}
