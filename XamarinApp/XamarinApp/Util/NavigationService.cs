﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using XamarinApp.Services;
using XamarinApp.ViewModels;

namespace XamarinApp.Util
{
    public class NavigationService : INavigationService
    {
        public async Task NavigateToAsync<TViewModel>() where TViewModel : BaseViewModel
        {
            await InternalNavigateToAsync<TViewModel>(typeof(TViewModel), null, false);
        }

        public async Task NavigateToAsync<TViewModel>(object parameters, bool modal = false) where TViewModel : BaseViewModel
        {
            await InternalNavigateToAsync<TViewModel>(typeof(TViewModel), parameters, modal);
        }

        public async Task PopAsync()
        {
            var MainPage = Application.Current.MainPage as NavigationPage;
            await MainPage.PopAsync();
        }

        private Type GetPageTypeForViewModel(Type viewModelType)
        {
            var viewName = viewModelType.FullName.Replace("Model", string.Empty);
            var viewModelAssemblyName = viewModelType.GetTypeInfo().Assembly.FullName;
            var viewAssemblyName = string.Format(CultureInfo.InvariantCulture, "{0}, {1}",
                viewName, viewModelAssemblyName);
            var viewType = Type.GetType(viewAssemblyName);
            return viewType;
        }

        private Page CreatePage(Type viewModelType)
        {
            Type pageType = GetPageTypeForViewModel(viewModelType);
            if (pageType == null)
            {
                throw new Exception($"Cannot locate page type for {viewModelType}");
            }

            Page page = Activator.CreateInstance(pageType) as Page;
            return page;
        }

        private async Task InternalNavigateToAsync<TViewModel>(Type viewModelType, object parameter, bool modal = false)
        {
            Page page = CreatePage(viewModelType);

            (page.BindingContext as BaseViewModel).Initialize(page);

            if (Application.Current.MainPage is NavigationPage navigationPage)
            {
                if (modal)
                {
                    await navigationPage.Navigation.PushModalAsync(page, true);
                }
                else
                {
                    await navigationPage.PushAsync(page);
                }
            }
            else
            {
                Application.Current.MainPage = new NavigationPage(page);
            }

            await (page.BindingContext as BaseViewModel).InitializeAsync(parameter);

        }
    }
}
